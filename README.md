# ase-testing

Datafiles and configuration information for testing ASE.

The purpose is to establish a standard set of pseudopotentials for ASE 
calculators.  With this set, it should be possible run simple tests 
without burdening the tester with finding pseudopotentials, and 
definitely without burdening the user with finding them.

Calculator tests in ASE then depend on this package so we have one
well-defined environment where any test should pass.

We try to provide datafiles for the following elements:

H  C  N  O
Al Si
Ti Fe Ni Cu Pd Ag Pt Au
